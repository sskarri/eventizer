/**
 * 
 */
package com.eventizer.application;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.eventizer.application.config.ResourceBuldlerConfig;
import com.eventizer.application.entity.Role;
import com.eventizer.application.entity.User;
import com.eventizer.application.services.UserService;

/**
 * @author shysatya
 *
 */
@SpringBootApplication(scanBasePackages={"com.eventizer.application.*"})
@CrossOrigin
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class EventizerApplication extends SpringBootServletInitializer
		implements ServletContainerInitializer, CommandLineRunner{


	  @Autowired
	  UserService userService;

	  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(EventizerApplication.class, args);

	}

	@Override
	public void onStartup(Set<Class<?>> arg0, ServletContext arg1) throws ServletException {
		// TODO Auto-generated method stub

	}

	public static final DateTimeFormatter FORMATTER = ofPattern("dd-MM-yyyy HH:MM");

	public static final DateTimeFormatter TIMEFORMATTER = ofPattern("HH:MM");

	/*
	 * @Bean
	 * 
	 * @Primary public ObjectMapper serializingObjectMapper() { ObjectMapper
	 * objectMapper = new ObjectMapper();
	 * 
	 * JavaTimeModule javaTimeModule = new JavaTimeModule();
	 * javaTimeModule.addSerializer(LocalDateTime.class, new
	 * LocalDateSerializer());
	 * javaTimeModule.addDeserializer(LocalDateTime.class, new
	 * LocalDateDeserializer()); objectMapper.registerModule(javaTimeModule);
	 * return objectMapper; }
	 */

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EventizerApplication.class);
	}
	
	@Bean
	public javax.validation.Validator getValidator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		validator.setValidationMessageSource(ResourceBuldlerConfig.messageSource());
		return validator;
	}
	
	 @Bean
	  public ModelMapper modelMapper() {
	    return new ModelMapper();
	  }

	@Override
	  public void run(String... params) throws Exception {
	   /* User admin = new User();
	    admin.setUsername("admin");
	    admin.setPassword("admin");
	    admin.setEmail("admin@email.com");
	    admin.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_DIRECTOR)));
	    userService.signup(admin);
	    User client = new User();
	    client.setUsername("client");
	    client.setPassword("client");
	    client.setEmail("client@email.com");
	    client.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_TEAM_MEMBER)));
	    userService.signup(client);*/
	  }

	 @Bean
	  public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder(12);
	  }

}
