/**
 * 
 */
package com.eventizer.application.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.stereotype.Component;

/**
 * @author shysatya
 *
 */
@Component
public class CommonUtils {

	public Date formatter(String date) throws ParseException {

		DateFormat readFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
		DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat readFinal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date date1 = null;
		try {
			date1 = readFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (date1 != null) {
			String formattedDate = writeFormat.format(date1);
		//	System.out.println(formattedDate);
			return readFinal.parse(formattedDate);
		}
		return null;
	}

	public Optional filterListWithValue(List obj, long key) {
		return obj.parallelStream().filter(e -> e.equals(key)).findFirst();
	}

}
