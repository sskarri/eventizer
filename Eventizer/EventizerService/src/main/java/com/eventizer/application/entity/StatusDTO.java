/**
 * 
 */
package com.eventizer.application.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author shysatya
 *
 */
@Entity
@Table(name="StatusDTO")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class StatusDTO {

	public StatusDTO(Date date, Status status,long key) {
		super();
		this.date = date;
		this.status = status;
		this.masterKey=key;
	}
	@OneToOne
	@JoinColumn(name = "master_id")
	private EventMaster statusMaster;
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;
	
	@JsonSerialize(using=JsonDateSerializer.class)
	@JsonDeserialize(using=JsonDateDeSerializer.class)
	private Date date;
	
	private long masterKey;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	public StatusDTO() {
		
	}
	/**
	 * @return the statusMaster
	 */
	public EventMaster getStatusMaster() {
		return statusMaster;
	}
	/**
	 * @param statusMaster the statusMaster to set
	 */
	public void setStatusMaster(EventMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	
	/**
	 * @return the masterKey
	 */
	public long getMasterKey() {
		return masterKey;
	}
	/**
	 * @param masterKey the masterKey to set
	 */
	public void setMasterKey(long masterKey) {
		this.masterKey = masterKey;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
	
}
