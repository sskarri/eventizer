/**
 * 
 */
package com.eventizer.application.entity;
import java.io.IOException; 
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
/**
 * @author shysatya
 *
 */

@Component
public class CustomDateSerializer extends StdSerializer<Date> {
	  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SimpleDateFormat formatter 
      = new SimpleDateFormat("HH:MM");
 
    public CustomDateSerializer() {
        this(null);
    }
 
    public CustomDateSerializer(Class t) {
        super(t);
    }
     
    @Override
    public void serialize ( Date value, JsonGenerator gen, SerializerProvider arg2)
      throws IOException, JsonProcessingException {
        gen.writeString(formatter.format(value));
    }
}