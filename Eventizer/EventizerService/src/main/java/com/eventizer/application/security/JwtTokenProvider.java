package com.eventizer.application.security;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.eventizer.application.entity.Role;
import com.eventizer.application.exceptions.CustomException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {


	@Value("${security.jwt.token.secret-key:secret-key}")
	private String secretKey;

	@Value("${security.jwt.token.key-issuer:CapgeminiCorp}")
	private String keyIssuer;

	@Value("${security.jwt.token.expire-length:3600000}")
	private long validityInMilliseconds = 3600000; // 1h

	@Autowired
	private MyUserDetails myUserDetails;

	
	@PostConstruct
	protected void init() {
		secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
	}

	public ArrayNode createToken(String username, List<Role> roles) {

		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		ObjectNode obj = mapper.createObjectNode(); 
		
		Claims claims = Jwts.claims().setSubject(username);
		claims.put("auth", roles.stream().map(s -> new SimpleGrantedAuthority(s.getAuthority()))
				.filter(Objects::nonNull).collect(Collectors.toList()));

		Date now = new Date();
		Date validity = new Date(now.getTime() + validityInMilliseconds * 4);
		LocalDateTime currentTime = LocalDateTime.now();

		String token = Jwts.builder().setClaims(claims).setIssuer(keyIssuer)
				.setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant())).setExpiration(validity)
				.signWith(SignatureAlgorithm.HS512, secretKey).compact();

		
		obj.put("token", token);
		obj.put("validUpto", validity.toString());
		arrayNode.add(obj);
		return arrayNode;

	}

	public String createToken1(String username, List<Role> roles) {

		Claims claims = Jwts.claims().setSubject(username);
		claims.put("auth", roles.stream().map(s -> new SimpleGrantedAuthority(s.getAuthority()))
				.filter(Objects::nonNull).collect(Collectors.toList()));

		LocalDateTime currentTime = LocalDateTime.now();
		Date now = new Date();
		Date validity = new Date(now.getTime() + validityInMilliseconds);

		String token = Jwts.builder().setClaims(claims).setIssuer(keyIssuer)
				.setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant())).setExpiration(validity)
				.signWith(SignatureAlgorithm.HS512, secretKey).compact();

		return token;
	}

	/*
	 * public String createToken2(String username, List<Role> roles) {
	 * 
	 * Claims claims = Jwts.claims().setSubject(username); claims.put("auth",
	 * roles.stream().map(s -> new
	 * SimpleGrantedAuthority(s.getAuthority())).filter(Objects::nonNull).
	 * collect(Collectors.toList())); LocalDateTime currentTime =
	 * LocalDateTime.now();
	 * 
	 * 
	 * String token = Jwts.builder() .setClaims(claims) .setIssuer(keyIssuer)
	 * .setId(UUID.randomUUID().toString())
	 * .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).
	 * toInstant())) .setExpiration(Date.from(currentTime
	 * .plusMinutes(settings.getRefreshTokenExpTime())
	 * .atZone(ZoneId.systemDefault()).toInstant()))
	 * .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
	 * .compact();
	 * 
	 * return new AccessJwtToken(token, claims); }
	 */

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = myUserDetails.loadUserByUsername(getUsername(token));
		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	}

	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
	}

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
			return true;
		} catch (JwtException | IllegalArgumentException e) {
			throw new CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
