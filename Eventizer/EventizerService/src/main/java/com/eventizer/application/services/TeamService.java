/**
 * 
 */
package com.eventizer.application.services;

import java.util.List;

import org.springframework.stereotype.Component;

import com.eventizer.application.entity.Team;

/**
 * @author shysatya
 *
 */
@Component
public interface TeamService {
	
	List<Team> getAllData();
	Team getTeamById(long masterId);
     boolean addTeam(Team event);
     void updateTeam(Team event);
     void deleteTeam(int teamId);
	

}
