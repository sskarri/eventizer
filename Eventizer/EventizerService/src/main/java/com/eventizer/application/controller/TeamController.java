/**
 * 
 */
package com.eventizer.application.controller;

import java.util.Arrays;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.eventizer.application.annotations.AllowedForDirector;
import com.eventizer.application.annotations.AllowedForManager;
import com.eventizer.application.annotations.AllowedForScrumMaster;
import com.eventizer.application.annotations.AllowedForTeamLead;
import com.eventizer.application.entity.Response;
import com.eventizer.application.entity.Team;
import com.eventizer.application.services.TeamService;

import io.swagger.annotations.Api;

/**
 * @author shysatya
 *
 */
@RestController
@RequestMapping("/api/teams")
@Api(tags = "TeamsController")
public class TeamController {

	@Autowired
	TeamService tServ;
	
	@AllowedForDirector
	@AllowedForManager
	@RequestMapping(value="/addTeam",method=RequestMethod.POST)
	public ResponseEntity<Response> addTeam(@Valid @RequestBody Team team, UriComponentsBuilder ucBuilder,
			BindingResult result) {

		tServ.addTeam(team);
		Response resp = new Response();
		resp.setMessage("Team has Successfully added");
		resp.setStatus(true);
		resp.setResult(Arrays.asList(team));
		return ResponseEntity.ok(resp);
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<Response> getAllTeams() {

		Response resp = new Response();
		resp.setMessage("Teams are retrived Succesfully");
		resp.setStatus(true);
		resp.setResult(Arrays.asList(tServ.getAllData()));
		return ResponseEntity.ok(resp);

	}


}
