/**
 * 
 */
package com.eventizer.application.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.access.prepost.PreAuthorize;

/**
 * @author shysatya
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize(AllowedForDirector.condition)
public @interface AllowedForDirector {
	
	String condition  = "hasRole('ROLE_DIRECTOR')";

}
