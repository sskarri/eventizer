/**
 * 
 */
package com.eventizer.application.entity;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * @author shysatya
 *
 */
@Component
public class Response {
	
	/**
	 * @return the status
	 */
	
	public Response(){}
	public boolean isStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the result
	 */
	public List getResult() {
		return result;
	}
	/**
	 * @param <T>
	 * @param result the result to set
	 */
	public <T> void setResult(List<T> result) {
		this.result = result;
	}
	private boolean status;
	private String message;
	private List result;
	
	


}
