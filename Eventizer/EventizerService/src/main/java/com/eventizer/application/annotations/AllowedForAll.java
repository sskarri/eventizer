/**
 * 
 */
package com.eventizer.application.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.access.prepost.PreAuthorize;

/**
 * @author shysatya
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize(AllowedForAll.condition)
public @interface AllowedForAll {
	
	String condition  = "hasAnyRole('ROLE_DIRECTOR','ROLE_MANAGER','ROLE_SCRUM_MASTER','ROLE_TEAM_LEAD','ROLE_TEAM_MEMBER')";
}