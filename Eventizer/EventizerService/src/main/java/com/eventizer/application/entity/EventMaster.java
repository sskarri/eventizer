/**
 * 
 */
package com.eventizer.application.entity;

import java.io.Serializable; 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author shysatya
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name="EVENT_MASTER",uniqueConstraints={@UniqueConstraint(columnNames={"name"})})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class EventMaster implements Serializable{

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private long id;
	/**
	 * @return the status
	 */


	@Column
	@NotBlank(message="{validationMsgSrc.blank}")
	private String name;
	@Column
	@NotBlank(message="{validationMsgSrc.blank}")
	private String location;
	
	@Column
	@Range(min=5, max=2000,message="{validationMsgSrc.range}")
	private long capacity;
	
	
	@OneToMany(mappedBy="master")
	 @JsonBackReference
	List<Event> events = new ArrayList();
	
	@OneToOne(mappedBy="statusMaster")
	StatusDTO stats;
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EventMaster [name=" + name + ", location=" + location + ", capacity=" + capacity + ", events=" + events
				+ ", stats=" + stats + "]";
	}
	/**
	 * @return the stats
	 */
	public StatusDTO getStats() {
		return stats;
	}
	/**
	 * @param stats the stats to set
	 */
	public void setStats(StatusDTO stats) {
		this.stats = stats;
	}
	/**
	 *                                                 @return the events
	 */
	public List<Event> getEvents() {
		return events;
	}
	/**
	 * @param events the events to set
	 */
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	/**
	 * @param events the events to set
	 */
	/*public void addEvent(Event events) {
		this.events.add(events);
	}
	public void removeEvent(Event events) {
		this.events.remove(events);
	}*/

	@Column
	 @Temporal(TemporalType.DATE)
	    @CreationTimestamp
	    private Date eventCreatedDate;

		/**
	 * @return the eventCreatedDate
	 */
	public Date getEventCreatedDate() {
		return eventCreatedDate;
	}
	/**
	 * @param eventCreatedDate the eventCreatedDate to set
	 */
	public void setEventCreatedDate(Date eventCreatedDate) {
		this.eventCreatedDate = eventCreatedDate;
	}
	/**
	 * @return the eventModifiedDate
	 */
	public Date getEventModifiedDate() {
		return eventModifiedDate;
	}
	/**
	 * @param eventModifiedDate the eventModifiedDate to set
	 */
	public void setEventModifiedDate(Date eventModifiedDate) {
		this.eventModifiedDate = eventModifiedDate;
	}


		@Column
		@Temporal(TemporalType.DATE)
	    @UpdateTimestamp
	    private Date eventModifiedDate;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the capacity
	 */
	public long getCapacity() {
		return capacity;
	}
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(long capacity) {
		this.capacity = capacity;
	}
	
	
	private static final long serialVersionUID = 1L;
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (capacity ^ (capacity >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventMaster other = (EventMaster) obj;
		if (capacity != other.capacity)
			return false;
		if (id != other.id)
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
