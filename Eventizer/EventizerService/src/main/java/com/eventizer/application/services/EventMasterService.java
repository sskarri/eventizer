/**
 * 
 */
package com.eventizer.application.services;

import java.util.List;

import org.springframework.stereotype.Component;

import com.eventizer.application.entity.EventMaster;

/**
 * @author shysatya
 *
 */
@Component
public interface EventMasterService {
		
	List<EventMaster> getAllData();
	EventMaster getEventMasterById(long masterId);
     boolean addEventMaster(EventMaster event);
     void updateEventMaster(EventMaster event);
     void deleteEventMaster(int masterId);
	
}
